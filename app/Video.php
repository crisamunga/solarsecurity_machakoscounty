<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model {

    protected $tablename = "videos";

    protected $guarded = [];

    public function alert()
    {
        return $this->belongsTo('App\Alert','alert_id');
    }
}
