<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolarPanel extends Model {

    protected $tablename = "solar_panels";

    protected $guarded = [];

    public function solar_installation()
    {
        return $this->belongsTo('App\SolarInstallation','solar_installation_id');
    }
}
