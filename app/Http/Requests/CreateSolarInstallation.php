<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSolarInstallation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:50|unique:solar_installations,name',
            'location_description' => 'required|string|max:250',
            'latitude' => 'required|string|max:10',
            'longitude' => 'required|string|max:10',
            'picture' => 'required|image',
        ];
    }
}
