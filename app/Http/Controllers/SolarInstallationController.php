<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SolarInstallation;
use Validator;
use App\Http\Requests\CreateSolarInstallation;

class SolarInstallationController extends Controller
{
    public function index()
    {
        $data = SolarInstallation::withCount('alerts')->get();
        return view('solarinstallations.index', compact("data"));
    }

    public function create()
    {
        return view('solarinstallations.create');
    }

    public function store(Request $request)
    {
        $input = $request->validate([
            'name' => 'required|string|max:50|unique:solar_installations,name',
            'location_description' => 'required|string|max:250',
            'latitude' => 'required|string|max:15',
            'longitude' => 'required|string|max:15',
            'picture' => 'required|image',
        ]);
        $storagepath = $request->file('picture')->store('public');
        $input['picture'] = $storagepath;
        $data = SolarInstallation::create($input);
        return view('solarinstallations.show', compact("data"));
    }

    public function show($id)
    {
        $data = SolarInstallation::with(['cameras','alert_contacts','alerts'])->findOrFail($id);
        return view('solarinstallations.show', compact("data"));
    }

    public function edit($id)
    {
        $data = SolarInstallation::findOrFail($id);
        return view('solarinstallations.edit', compact("data"));
    }

    public function update(Request $request, $id)
    {
        $data = SolarInstallation::findOrFail($id);
        $input = $input = $request->validate([
            'name' => 'required|string|max:50|unique:solar_installations,name,'.$data->id,
            'location_description' => 'required|string|max:250',
            'latitude' => 'required|string|max:15',
            'longitude' => 'required|string|max:15',
            'picture' => 'image',
        ]);
        $file = $request->file('picture');
        if (isset($file)) {
            $storagepath = $request->file('picture')->store('public');
            $input['picture'] = $storagepath;
        } else {
            unset($input['picture']);
        }
        $data->update($input);
        return redirect()->route('solarinstallations.show', ['id' => $id]);
    }

    public function destroy($id)
    {
        $row = SolarInstallation::findOrFail($id);
        $row->delete();
        return redirect()->route('solarinstallations.index');
    }

    public function secure(Request $request, $id)
    {
        $data = SolarInstallation::findOrFail($id)->update(['alert_status'=>false]);
        return redirect()->route('solarinstallations.show', ['id' => $id]);
    }
}
