<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Camera extends Model {

    protected $tablename = "cameras";

    protected $guarded = [];

    public function solar_installation()
    {
        return $this->belongsTo('App\SolarInstallation','solar_installation_id');
    }
}
