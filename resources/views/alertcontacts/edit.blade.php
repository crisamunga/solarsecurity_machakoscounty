@extends('layouts.admin')

@section('admincontent')
    
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="flex-wrap">
                    <div class="card">
                        {{-- <div class="card-header">{{ __('Register') }}</div> --}}
                        <div class="card-header">
                            <a href="/">
                                <img src="/images/icon/logo-mini.png" alt="Solar Security">
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="card-title">
                                <h3 class="text-center title-2">Edit {{$data->name}}</h3>
                            </div>
                            <form method="POST" action="{{ route('alertcontacts.update', $data->id) }}">
                                @csrf
                                @method('PUT')
                                @forminputfields(['fieldname' => 'name', 'fieldtype' => 'text', 'attributes' => 'required autofocus', 'fielddefault' => $data->name])

                                @forminputfields(['fieldname' => 'email', 'fieldtype' => 'email', 'attributes' => 'required', 'fielddefault' => $data->email])

                                @forminputfields(['fieldname' => 'phone_number', 'fieldlabel' => 'Phone number', 'fieldtype' => 'text', 'attributes' => 'required', 'fielddefault' => $data->phone_number])

                                <div class="form-group row">
                                    <p class="text-center">
                                        @if ($errors->has('solar_installation_ids.*'))
                                            <span class="text-danger error">
                                                <strong>{{ $errors->first('solar_installation_ids.*') }}</strong>
                                            </span>
                                        @endif
                                    </p>
                                    <div class="table-responsive m-b-40">
                                        <table class="table table-borderless table-data2 datatable">
                                            <thead>
                                                <tr>
                                                    <th>Solar installation</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($solarinstallations as $solarinstallation)
                                                    <tr>
                                                        <td>
                                                            @forminputfields(['fieldname' => 'solar_installation_ids[]', 'fieldlabel' => $solarinstallation->name, 'fieldtype' => 'checkbox', 'fieldvalue' => $solarinstallation->id, 'attributes' => 'required'])
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                @forminputfields(['fieldtype' => 'submit', 'fieldlabel' => 'Add alert contact'])
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>

@endsection
