<ul class="list-unstyled navbar__list">
    <li>
        <a href="{{route('dashboard.index')}}"><i class="fas fa-tachometer-alt"></i>Dashboard</a>
    </li>
    <li>
        <a href="{{route('alerts.index')}}"><i class="fas fa-warning"></i>Alerts</a>
    </li>
    <li>
        <a href="{{route('solarinstallations.index')}}"><i class="fas fa-sun"></i>Solar Installations</a>
    </li>
    <li>
        <a href="{{route('alertcontacts.index')}}"><i class="fas fa-user"></i>Alert contacts</a>
    </li>
</ul>