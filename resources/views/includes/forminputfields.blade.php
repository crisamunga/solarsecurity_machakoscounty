@if ($fieldtype == 'submit')
    <div class="form-group row mb-0">
        <div class="col-md-8 offset-md-2">
            <button type="submit" class="btn btn-primary btn-block">
                    {{ __(isset($fieldlabel) ? $fieldlabel : 'Submit') }}
            </button>
        </div>
    </div>
@else
    <div class="form-group row">
        <label for="{{isset($fieldid) ? $fieldid : $fieldname}}" class="col-md-4 col-form-label text-md-right">{{ __(isset($fieldlabel) ? $fieldlabel : ucfirst($fieldname)) }}</label>

        <div class="col-md-6">
            @if($fieldtype == 'image')
                <div class="col-sm-12 imgUp">
                    <div class="imagePreview"></div>
                    <label class="btn btn-dark">
                        Choose file to upload
                        <input type="file" class="uploadFile img{{ $errors->has($fieldname) ? ' is-invalid' : '' }}" value="Upload Image" style="width: 0px;height: 0px;overflow: hidden;" id="{{isset($fieldid) ? $fieldid : $fieldname}}" name="{{$fieldname}}" {{isset($attributes) ? $attributes : ''}}>
                    </label>
                </div>
            @elseif($fieldtype == 'radio')
                <label class="switch switch-3d switch-primary mr-3">
                    <input id="{{isset($fieldid) ? $fieldid : $fieldname}}" type="radio" class="switch-input form-control{{ $errors->has($fieldname) ? ' is-invalid' : '' }}" {{(isset($fielddefault) && $fielddefault) ? 'checked' : ''}} name="{{$fieldname}}"  value="{{$fieldvalue}}" {{isset($attributes) ? $attributes : ''}}>
                    <span class="switch-label"></span>
                    <span class="switch-handle"></span>
                </label>
            @elseif($fieldtype == 'checkbox')
                <label class="switch switch-3d switch-primary mr-3">
                    <input id="{{isset($fieldid) ? $fieldid : $fieldname}}" type="checkbox" class="switch-input form-control{{ $errors->has($fieldname) ? ' is-invalid' : '' }}" {{(isset($fielddefault) && $fielddefault) ? 'checked' : ''}} name="{{$fieldname}}" value="{{$fieldvalue}}" {{isset($attributes) ? $attributes : ''}}>
                    <span class="switch-label"></span>
                    <span class="switch-handle"></span>
                </label>
            @elseif($fieldtype == 'select')
                <select id="{{isset($fieldid) ? $fieldid : $fieldname}}" class="form-control{{ $errors->has($fieldname) ? ' is-invalid' : '' }}" name="{{$fieldname}}" {{isset($attributes) ? $attributes : ''}}>
                        <option value="-1">{{ __(isset($fieldlabel) ? $fieldlabel : ucfirst($fieldname)) }}</option>    
                    @foreach ($fieldoptions as $optionvalue => $optionlabel)
                        <option value="{{$optionvalue}}" {{ (isset($fielddefault) && $fielddefault == $optionvalue) ? 'selected' : '' }}>{{$optionlabel}}</option>
                    @endforeach
                </select>
            @elseif($fieldtype == 'textarea')
                <textarea id="{{isset($fieldid) ? $fieldid : $fieldname}}" type="{{$fieldtype}}" class="au-input au-input--full form-control{{ $errors->has($fieldname) ? ' is-invalid' : '' }}" name="{{$fieldname}}" {{isset($attributes) ? $attributes : ''}}>{{ isset($fielddefault) ? $fielddefault : old($fieldname) }}</textarea>
            @else
                <input id="{{isset($fieldid) ? $fieldid : $fieldname}}" type="{{$fieldtype}}" class="au-input au-input--full form-control{{ $errors->has($fieldname) ? ' is-invalid' : '' }}" name="{{$fieldname}}" value="{{ isset($fielddefault) ? $fielddefault : old($fieldname) }}" {{isset($attributes) ? $attributes : ''}}>
            @endif

            @if ($errors->has($fieldname))
                <span class="text-danger error">
                    <strong>{{ $errors->first($fieldname) }}</strong>
                </span>
            @endif
        </div>
    </div>    
@endif