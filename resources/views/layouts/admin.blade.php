@extends('layouts.app')

@section('content')
    {{-- Admin sidebar --}}
    
    <aside class="menu-sidebar2">
        <div class="logo">
            <a href="/">
                <img src="/images/icon/logo-white-large.png" alt="Solar Security" />
            </a>
        </div>
        <div class="menu-sidebar2__content js-scrollbar1">
            <div class="account2">
                <div class="image img-cir img-120">
                    <img src="/images/icon/avatar-big-01.jpg" alt="John Doe" />
                </div>
                <h4 class="name">john doe</h4>

                @logoutlink
            </div>
            <nav class="navbar-sidebar2">
                @include('includes.adminsidebar')
            </nav>
        </div>
    </aside>

    {{-- End of Admin sidebar --}}

    {{-- Page Container --}}
    <div class="page-container2">

        <!-- HEADER DESKTOP-->
        <header class="header-desktop2">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="header-wrap2">
                        <div class="logo d-block d-lg-none">
                            <a href="/">
                                <img src="/images/icon/logo-white.png" alt="CoolAdmin" />
                            </a>
                        </div>
                        <div class="header-button2">
                            <div class="header-button-item js-item-menu">
                                <i class="zmdi zmdi-search"></i>
                                <div class="search-dropdown js-dropdown">
                                    <form action="">
                                        <input class="au-input au-input--full au-input--h65" type="text" placeholder="Search for datas &amp; reports..." />
                                        <span class="search-dropdown__icon">
                                            <i class="zmdi zmdi-search"></i>
                                        </span>
                                    </form>
                                </div>
                            </div>
                            <div class="header-button-item mr-0 js-sidebar-btn">
                                <i class="zmdi zmdi-menu"></i>
                            </div>
                            <div class="setting-menu js-right-sidebar d-none d-lg-block">
                                <div class="account-dropdown__body">
                                    <div class="account-dropdown__item">
                                        <a href="#">
                                            <i class="zmdi zmdi-account"></i>Account</a>
                                    </div>
                                    <div class="account-dropdown__item">
                                        <a href="#">
                                            <i class="zmdi zmdi-settings"></i>Setting</a>
                                    </div>
                                    <div class="account-dropdown__item">
                                        @logoutlink
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <aside class="menu-sidebar2 js-right-sidebar d-block d-lg-none">
            <div class="logo">
                <a href="/">
                    <img src="/images/icon/logo-white.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar2__content js-scrollbar2">
                <div class="account2">
                    <div class="image img-cir img-120">
                        <img src="/images/icon/avatar-big-01.jpg" alt="John Doe" />
                    </div>
                    <h4 class="name">john doe</h4>
                    <a href="#">Sign out</a>
                </div>
                <nav class="navbar-sidebar2">
                    @include('includes.adminsidebar')
                </nav>
            </div>
        </aside>

        @yield('admincontent')
        <!-- END HEADER DESKTOP-->
    </div>

    {{-- End of page Container --}}
@endsection

@section('scripts')
    <script>
        $(function(){
            var pathname = window.location.origin+"/"+window.location.pathname.split('/')[1];
            $('a[href="'+ pathname +'"]').parent().addClass('active');

            $('.datatable').each((index, elem) => {
                $(elem).DataTable();
            });
        });
    </script>
    @yield('adminscripts')
@endsection