@extends('layouts.admin')

@section('admincontent')
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <h2 class="text-center"><a href="{{route('solarinstallations.show',$data->solar_installation->id)}}">{{$data->solar_installation->name}}</a> {{": ".$data->created_at}}</h2>
                <h3 class="m-t-40 text-center">Images</h3>
                <div class="row m-t-30">
                    @foreach ($data->images as $image)
                        <div class="col-sm-4">
                            <div class="image img-fluid">
                                <img class="imagePreview" src="{{asset(Storage::url($image->path))}}" alt="Alert image" />
                            </div>
                        </div>
                    @endforeach
                </div>
                <h3 class="m-t-40 text-center">Videos</h3>
                <div class="row m-t-30">
                    @foreach ($data->videos as $video)
                        <div class="col-lg-6 align-content-center m-0 p-0">
                            <video height="240" controls>
                                <source src="{{Storage::url($video->path)}}" type="video/mp4">
                            </video>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
