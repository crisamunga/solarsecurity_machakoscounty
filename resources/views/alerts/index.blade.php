    @extends('layouts.admin')

    @section('admincontent')
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row m-t-30">
                        <div class="col-md-12">
                            <div class="table-data__tool">
                                <div class="table-data__tool-left">
                                    <h3 class="title-5 m-b-35">Raised alerts</h3>
                                </div>
                            </div>
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-data2 datatable">
                                    <thead>
                                        <tr>
                                            <th>Date and time</th>
                                            <th>Solar installation</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $alert)
                                            <tr>
                                                <td>{{$alert->created_at}}</td>
                                                <td>
                                                    <a href="{{route('solarinstallations.show',$alert->solar_installation->id)}}">{{$alert->solar_installation->name}}</a>
                                                </td>
                                                <td>
                                                    <a href="{{route('alerts.show', $alert->id)}}" class="item" data-toggle="tooltip" data-placement="top" title="More">
                                                        <i class="zmdi zmdi-more"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
