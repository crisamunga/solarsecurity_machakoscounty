@extends('layouts.admin')

@section('admincontent')
    
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="flex-wrap">
                    <div class="card">
                        {{-- <div class="card-header">{{ __('Register') }}</div> --}}
                        <div class="card-header">
                            <a href="/">
                                <img src="/images/icon/logo-mini.png" alt="Solar Security">
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="card-title">
                                <h3 class="text-center title-2">Add camera</h3>
                            </div>
                            <form method="POST" action="{{ route('cameras.store') }}" enctype="multipart/form-data">
                                @csrf

                                @forminputfields(['fieldname' => 'name', 'fieldtype' => 'text', 'attributes' => 'required autofocus'])

                                @forminputfields(['fieldname' => 'address', 'fieldtype' => 'text', 'attributes' => 'required'])

                                @forminputfields(['fieldname' => 'solar_installation_id', 'fieldtype' => 'select', 'fieldlabel' => 'Select solar installation', 'fieldoptions' => $solarinstallations->pluck('name','id')])

                                @forminputfields(['fieldtype' => 'submit', 'fieldlabel' => 'Add camera'])
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>

@endsection
