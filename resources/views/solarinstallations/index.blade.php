    @extends('layouts.admin')

    @section('admincontent')
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row m-t-30">
                        <div class="col-md-12">
                            <!-- DATA TABLE-->
                            <div class="table-data__tool">
                                <div class="table-data__tool-left">
                                    <h3 class="title-5 m-b-35">Solar Installations</h3>
                                </div>
                                <div class="table-data__tool-right">
                                    <a href="{{route('solarinstallations.create')}}" class="btn au-btn au-btn-icon au-btn--green au-btn--small">
                                        <i class="zmdi zmdi-plus"></i>Add Item
                                    </a>
                                </div>
                            </div>
                            <div class="table-responsive m-b-40">
                                <table class="table table-borderless table-data2 datatable">
                                    <thead>
                                        <tr>
                                            <th>Status</th>
                                            <th>Name</th>
                                            <th>Location</th>
                                            <th>Alerts raised</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $solarinstallation)
                                            <tr>
                                                @if ($solarinstallation->alert_status)
                                                    <td class="text-danger">Alarm raised</td>
                                                @else
                                                    <td class="text-success">Secure</td>
                                                @endif
                                                <td>{{$solarinstallation->name}}</td>
                                                <td>
                                                    <a href="{{$solarinstallation->google_map_url}}" target="_blank">{{$solarinstallation->location_description}}</a>
                                                </td>
                                                <td>
                                                    {{$solarinstallation->alerts_count}}
                                                </td>
                                                <td>
                                                    <div class="table-data-feature">
                                                        <a href="{{route('solarinstallations.edit', $solarinstallation->id)}}" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                            <i class="zmdi zmdi-edit"></i>
                                                        </a>
                                                        <form action="{{route('solarinstallations.destroy', $solarinstallation->id)}}" method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <i class="zmdi zmdi-delete"></i>
                                                            </button>
                                                        </form>
                                                        <a href="{{route('solarinstallations.show', $solarinstallation->id)}}" class="item" data-toggle="tooltip" data-placement="top" title="More">
                                                            <i class="zmdi zmdi-more"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
