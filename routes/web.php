<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');


Route::resource('/alertcontacts', 'AlertContactController');
Route::resource('/alerts', 'AlertController', ['only' => ['index','show']]);
Route::resource('/cameras', 'CameraController');
Route::resource('/solarinstallations', 'SolarInstallationController');
Route::post('/solarinstallations/{id}/secure', 'SolarInstallationController@secure')->name('solarinstallations.secure');
Route::resource('/solarpanels', 'SolarPanelController');
Route::resource('/users', 'UserController');
Route::resource('/videos', 'VideoController');