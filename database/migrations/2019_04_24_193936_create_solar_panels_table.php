<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolarPanelsTable extends Migration
{
    public function up()
    {
        Schema::create('solar_panels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',50);
            $table->string('serial',100)->unique();
            $table->boolean('available')->default(true);
            $table->bigInteger('solar_installation_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('solar_panels');
    }
}
