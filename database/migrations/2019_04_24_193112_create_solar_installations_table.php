<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolarInstallationsTable extends Migration
{
    public function up()
    {
        Schema::create('solar_installations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',50);
            $table->string('picture',150);
            $table->string('location_description',250)->nullable();
            $table->string('latitude');
            $table->string('longitude');
            $table->boolean('alert_status')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('solar_installations');
    }
}
