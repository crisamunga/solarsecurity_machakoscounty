<?php

use App\Alert;
use Faker\Generator as Faker;

$factory->define(Alert::class, function (Faker $faker) {
return [
'key' => Str::slug($faker->sentence),
'value' => $faker->sentence,
];
});
