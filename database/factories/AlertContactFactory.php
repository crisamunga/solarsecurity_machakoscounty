<?php

use App\AlertContact;
use Faker\Generator as Faker;

$factory->define(AlertContact::class, function (Faker $faker) {
    return [
        'key' => Str::slug($faker->sentence),
        'value' => $faker->sentence,
    ];
});
