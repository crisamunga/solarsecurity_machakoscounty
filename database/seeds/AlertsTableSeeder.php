<?php

use App\Alert;
use Illuminate\Database\Seeder;

class AlertsTableSeeder extends Seeder
{
    public function run()
    {
        factory(Alert::class,200)->create();
}
}
