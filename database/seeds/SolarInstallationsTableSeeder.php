<?php

use app\SolarInstallation;
use Illuminate\Database\Seeder;

class SolarInstallationsTableSeeder extends Seeder
{
    public function run()
    {
        factory(SolarInstallation::class,10)->create();
    }
}
