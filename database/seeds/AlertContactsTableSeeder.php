<?php

use App\AlertContact;
use Illuminate\Database\Seeder;

class AlertContactsTableSeeder extends Seeder
{
    public function run()
    {
        factory(AlertContact::class,50)->create();
    }
}
